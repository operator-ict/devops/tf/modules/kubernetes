terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 4.2.0, < 5.0.0"
    }
  }
}

data "azurerm_client_config" "current" {}

resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name != null ? var.resource_group_name : "rg-aks-${var.name}"
  location = var.location
  tags     = var.tags
}

resource "azurerm_user_assigned_identity" "aks_identity" {
  name                = "aks-${var.name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags
}

resource "azurerm_role_assignment" "ra-aks" {
  scope                = azurerm_resource_group.rg.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_user_assigned_identity.aks_identity.principal_id
}

resource "azurerm_role_assignment" "ra-vnet" {
  scope                = var.vnet_id
  role_definition_name = "Contributor"
  principal_id         = azurerm_user_assigned_identity.aks_identity.principal_id
}

data "azurerm_public_ip" "pips" {
  for_each            = var.public_ip_names
  name                = each.key
  resource_group_name = var.public_ip_resource_group
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                   = var.name
  location               = var.location
  resource_group_name    = azurerm_resource_group.rg.name
  dns_prefix             = var.name
  kubernetes_version     = var.kubernetes_version
  local_account_disabled = true
  azure_policy_enabled   = true
  sku_tier               = var.sku_tier
  oidc_issuer_enabled    = var.enable_oidc
  # automatic_channel_upgrade: "patch"
  tags                   = var.tags

  default_node_pool {
    name                        = var.default_node_pool.name
    orchestrator_version        = var.kubernetes_version
    node_count                  = var.default_node_pool.enable_auto_scaling ? null : var.default_node_pool.node_count
    vm_size                     = var.default_node_pool.vm_size
    vnet_subnet_id              = var.subnet_id
    max_pods                    = var.default_node_pool.max_pods
    os_disk_type                = var.default_node_pool.os_disk_type
    auto_scaling_enabled        = var.default_node_pool.enable_auto_scaling
    min_count                   = var.default_node_pool.enable_auto_scaling ? var.default_node_pool.min_count : null
    max_count                   = var.default_node_pool.enable_auto_scaling ? var.default_node_pool.max_count : null
    zones                       = var.default_node_pool.zones
    temporary_name_for_rotation = "tmpdefault"

    upgrade_settings {
      drain_timeout_in_minutes      = 30
      node_soak_duration_in_minutes = 0
      max_surge                     = "33%"
    }

  }


  network_profile {
    network_plugin = "azure"
    service_cidr   = "172.20.0.0/22"
    dns_service_ip = "172.20.0.10"
    outbound_type  = var.outbound_type

    load_balancer_profile {
      outbound_ip_address_ids = length(data.azurerm_public_ip.pips) > 0 ? [
        element(values(data.azurerm_public_ip.pips), 0).id
      ] : []
    }
  }

  azure_active_directory_role_based_access_control {
    azure_rbac_enabled = true
    tenant_id = data.azurerm_client_config.current.tenant_id
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.aks_identity.id]
  }

  kubelet_identity {
    client_id                 = azurerm_user_assigned_identity.aks_identity.client_id
    object_id                 = azurerm_user_assigned_identity.aks_identity.principal_id
    user_assigned_identity_id = azurerm_user_assigned_identity.aks_identity.id
  }

  key_vault_secrets_provider {
    secret_rotation_enabled  = false
    secret_rotation_interval = "2m"
  }

  image_cleaner_interval_hours = 48

  dynamic "monitor_metrics" {
    for_each = (var.enable_metrics ? [1] : [])
    content {
    }
  }

  node_os_upgrade_channel = var.node_os_channel_upgrade

  dynamic "oms_agent" {
    for_each = (var.enable_monitoring ? [1] : [])
    content {
      log_analytics_workspace_id = var.logAnalyticsWorkspaceId
    }
  }
  depends_on = [azurerm_role_assignment.ra-aks, azurerm_role_assignment.ra-vnet, data.azurerm_public_ip.pips]
}

resource "azurerm_kubernetes_cluster_node_pool" "nodepools" {
  for_each              = var.node_pools
  name                  = each.key
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks.id
  vm_size               = each.value.vm_size
  node_count            = each.value.enable_auto_scaling ? null : each.value.node_count
  max_pods              = each.value.max_pods
  orchestrator_version  = var.kubernetes_version
  vnet_subnet_id        = var.subnet_id
  mode                  = each.value.mode
  node_taints           = each.value.node_taints
  node_labels           = each.value.node_labels
  auto_scaling_enabled  = each.value.enable_auto_scaling
  min_count             = each.value.enable_auto_scaling ? each.value.min_count : null
  max_count             = each.value.enable_auto_scaling ? each.value.max_count : null
  os_disk_type          = each.value.os_disk_type
  zones                 = each.value.zones
  tags                  = var.tags

  lifecycle {
    ignore_changes = [
      node_count,
    ]
  }
}

resource "azurerm_role_assignment" "pip-role" {
  for_each             = data.azurerm_public_ip.pips
  principal_id         = azurerm_user_assigned_identity.aks_identity.principal_id
  role_definition_name = "Network Contributor"
  scope                = each.value.id

  depends_on = [azurerm_user_assigned_identity.aks_identity]
}

module "prometheus" {
  source  = "gitlab.com/operator-ict/prometheus/azurerm"
  version = "0.1.0"

  count               = var.enable_metrics ? 1 : 0
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  name                = azurerm_kubernetes_cluster.aks.name
  target_resource_id  = azurerm_kubernetes_cluster.aks.id
}

data "azurerm_lb" "lb" {
  count               = var.private_link_service == null ? 0 : 1
  name                = var.private_link_service.load_balancer.name
  resource_group_name = var.private_link_service.load_balancer.resource_group_name
}

resource "azurerm_private_link_service" "pls" {
  count               = var.private_link_service == null ? 0 : 1
  name                = "aks-${azurerm_kubernetes_cluster.aks.name}-privatelink"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  auto_approval_subscription_ids              = var.private_link_service.auto_approval_subscription_ids
  visibility_subscription_ids                 = distinct(concat(var.private_link_service.visibility_subscription_ids, var.private_link_service.auto_approval_subscription_ids))
  load_balancer_frontend_ip_configuration_ids = [data.azurerm_lb.lb[0].frontend_ip_configuration[0].id]

  nat_ip_configuration {
    name      = "primary"
    subnet_id = var.subnet_id
    primary   = true
  }
}

resource "azurerm_private_endpoint" "pe" {
  for_each            = var.private_endpoints == null ? {} : var.private_endpoints
  name                = each.key
  location            = azurerm_kubernetes_cluster.aks.location
  resource_group_name = azurerm_kubernetes_cluster.aks.resource_group_name
  subnet_id           = var.subnet_id

  private_service_connection {
    name                           = each.key
    private_connection_resource_id = each.value.service_id
    is_manual_connection           = false
  }
  dynamic "ip_configuration" {
    for_each = each.value.static_ip == null ? [] : ["X"]
    content {
      name               = each.key
      private_ip_address = each.value.static_ip
    }
  }
}

resource "azurerm_role_assignment" "roles" {
  for_each = {
    for val in coalesce(var.roles, []) : "${val.role}>${val.principal_id}" => val
  }

  scope                = azurerm_kubernetes_cluster.aks.id
  role_definition_name = each.value.role
  principal_id         = each.value.principal_id
}
