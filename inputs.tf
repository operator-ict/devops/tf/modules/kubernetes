variable "name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "vnet_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "kubernetes_version" {
  type = string
}

variable "sku_tier" {
  type = string
}

variable "location" {
  type = string
}

variable "outbound_type" {
  type    = string
  default = "loadBalancer"
}
#
#variable "public_ip_id" {
#  type = string
#}

variable "log_analytics_workspace_id" {
  type    = string
  default = null
}

variable "enable_monitoring" {
  type    = bool
  default = false
}

variable "enable_oidc" {
  type    = bool
  default = false
}

variable "enable_metrics" {
  type    = bool
  default = false
}

variable "resource_group_name" {
  type    = string
  default = null
}

variable "public_ip_names" {
  type = map(string)
}

variable "public_ip_resource_group" {
  type = string
}

variable "enableMonitoring" {
  type    = bool
  default = false
}

variable "logAnalyticsWorkspaceId" {
  type    = string
  default = ""
}

variable "node_os_channel_upgrade" {
  type    = string
  default = "Unmanaged"
}

variable "default_node_pool" {
  type = object({
    vm_size             = string
    zones               = list(number)
    name                = optional(string, "default")
    max_pods            = optional(number, 150)
    node_count          = optional(number, 3)
    os_disk_type        = optional(string, "Ephemeral")
    enable_auto_scaling = optional(bool, false)
    min_count           = optional(number, 0)
    max_count           = optional(number, 5)
  })
}

variable "node_pools" {
  type = map(object({
    vm_size             = string
    node_count          = number
    zones               = list(number)
    max_pods            = optional(number, 150)
    mode                = optional(string, "User")
    node_taints         = optional(list(string), [])
    node_labels         = optional(map(string), {})
    enable_auto_scaling = optional(bool, false)
    min_count           = optional(number, 0)
    max_count           = optional(number, 5)
    os_disk_type        = optional(string, "Ephemeral")
  }))
  default = {}
}

variable "private_link_service" {
  type = object({
    load_balancer = object({
      name                = string
      resource_group_name = string
    })
    auto_approval_subscription_ids = optional(list(string), [])
    visibility_subscription_ids    = optional(list(string), [])
  })
  default = null
}

variable "private_endpoints" {
  type = map(object({
    service_id = string
    static_ip  = optional(string)
  }))
  default = null
}

variable "roles" {
  type = list(object({
    role         = string
    principal_id = string
  }))
  default = []
}
