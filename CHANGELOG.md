# Unreleased
## Added
## Changed
## Fixed


# [0.15.0] - 2025-01-03
## Added
- Add roles and priviledges

# [0.14.0] - 2024-09-18
## Changed
- Use Azurerm >=4.2.0

# [0.13.6] - 2024-07-30
## Changed
- Option for oidc issuer

# [0.13.4] - 2024-06-11
## Changed
- Include parameters for module

# [0.13.3] - 2024-06-03
## Changed
- Upgrade prometheus module version

# [0.13.2] - 2024-05-30
## Changed
- Move nsg to separate module

# [0.13.1] - 2023-03-03
## Added
- Add private_link_service and private_endpoints support

# [0.13.0] - 2023-08-28
## Added
- BC: Add zones for (default) node pools
- decrease default max_pods

# [0.12.1] - 2023-08-28
## Added
- Add node_os_channel_upgrade - https://learn.microsoft.com/en-gb/azure/aks/auto-upgrade-node-image

# [0.12.0] - 2023-08-16
## Added
- Introducing Azure prometheus

# [0.11.0] - 2023-08-11
## Changed
- Remove unused dependency on key-vault

# [0.10.3] - 2023-04-11
## Changed
- Removed deprecated attribute `docker_bridge_cidr`

# [0.10.2] - 2023-03-03
## Added
- Add enable_metrics

# [0.10.1] - 2023-02-20
## Added
- Add security rules for limit access to load balancer

# [0.10.0] - 2023-01-25
## Changed
- Disable `node_count` when `enable_auto_scaling` is `true`

# [0.9.0] - 2023-01-25
## Added
- Add autoscale to default node pool

# [0.8.1] - 2023-01-02
## Added
- Add tags

# [0.8.0] - 2022-11-10
## Added
- Add attribute os_disk_type
- Add autoscalling
## Changed
- BC: new object `default_node_pool` covers previous attributes `node_count`, `vm_size`
## Fixed

# [0.7.0] - 2022-11-10
## Added
## Changed
- BC: new object `default_node_pool` covers previous attributes `node_count`, `vm_size`
## Fixed

# [0.5.0] - 2022-11-03
## Added
- add basic node_pools
## Changed
## Fixed
