#output "principal_id" {
#  value = azurerm_user_assigned_identity.aks_identity.principal_id
#}

output "front_door_origin" {
  value = element(values(data.azurerm_public_ip.pips), 0).ip_address
}
